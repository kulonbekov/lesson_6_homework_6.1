package com.company;

public abstract class Player {

    private int zdorovie;
    private String name;

    public Player() {
    }

    public void printInfo() {

        System.out.println(getZdorovie() + " ");

    }

    public int getZdorovie() {
        return zdorovie;
    }

    public void setZdorovie(int zdorovie) {
        this.zdorovie = zdorovie;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}