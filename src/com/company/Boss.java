package com.company;

public class Boss extends Player {
    private int zashita;

    public int getZashita() {
        return zashita;
    }

    public void setZashita(int zashita) {
        this.zashita = zashita;
    }

    public Boss (int zashita, int zdorovie, String name){
        this.zashita=zashita;
        super.setZdorovie(zdorovie);
        super.setName(name);
    }
    public void printInfo() {

        System.out.println(super.getName() + " " + super.getZdorovie() + " " + zashita);
    }
}
