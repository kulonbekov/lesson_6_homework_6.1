package com.company;

public class Magical extends Player implements Superactivatable{

    private int magic;

    public int getMagic() {
        return magic;
    }

    public void setMagic(int magic) {
        this.magic = magic;
    }
    /*public Magical(int magic, int zdorovie, String name) {
        this.magic = magic;
        super.setZdorovie(zdorovie);
        super.setName(name);
    }*/
    public void printInfo() {

        System.out.println(super.getName() + " " + super.getZdorovie() + " " + magic);
    }

    @Override
    public void activate() {
        System.out.println("Активируем суперспособность Magical");
    }
}
