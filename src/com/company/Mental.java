package com.company;

public class Mental extends Player implements Superactivatable {
    private int attack;

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }
   /* public Mental(int attack, int zdorovie ,String name ) {
        this.attack = attack;
        super.setZdorovie(zdorovie);
        super.setName(name);
    }*/
    public void printInfo() {

        System.out.println(super.getName() + " " + super.getZdorovie() + " " + attack);
    }

    @Override
    public void activate() {
        System.out.println("Активируем суперспособность Mental");
    }
}
