package com.company;

public class Warrior extends Player implements Superactivatable {

    private int udar;

    public int getUdar() {
        return udar;
    }

    public void setUdar(int udar) {
        this.udar = udar;
    }

    /*public Warrior(int udar, int zdorovie, String name) {
        this.udar = udar;
        super.setZdorovie(zdorovie);
        super.setName(name);
    }*/

    public void printInfo() {

        System.out.println(super.getName() + " " + super.getZdorovie() + " " + udar);
    }

    @Override
    public void activate() {
        System.out.println("Активируем суперспособность Warrior");
    }
}